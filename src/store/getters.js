export default {

  getClients: function(state) {
    state.clients.forEach(client => {
      let date = new Date(client.adding_date);
      client.adding_date = date.toLocaleDateString("ru-RU", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit",
      });
    });
    return state.clients;
  },
  getClient: (state) => (index) => state.clients[index],
  getLimit: (state) => state.limit,

}