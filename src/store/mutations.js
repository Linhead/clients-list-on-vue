import Vue from 'vue'

export default {
  EDIT_CLIENT: (state, client, index) => {
    state.clients[index] = client;
  },
  /*ADD_CLIENT: (state, client) => {
    state.clients.push(client);
  },*/
  DELETE_CLIENT: (state, index) => {
    state.clients.splice(index, 1);
  },
  SET_CLIENTS: (state, { page, clients }) => {
    Vue.set(state.clients, page, clients);
    state.clients = clients;
  },
  SET_PAGES: (state, pages) => {
    state.pages = pages;
  },
  CHANGE_LIMIT: (state, limitNumber) => {
    state.limit = limitNumber
  },
  TOGGLE_LOADING: (state, value) => {
    state.loading = value;
  }
}