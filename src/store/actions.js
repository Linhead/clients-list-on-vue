import ClientService from '@/services/ClientService.js'

export default {
  editClient: ({
    commit
  }, {
    client,
    index
  }) => {
    return ClientService.updateClient(client).then(() => {
      commit("EDIT_CLIENT", client, index)
    })
  },
  createClient: ({
    commit
  }, client) => {
    console.log(commit);
    ClientService.postClient(client);
  },
  deleteClient: (ctx, index) => {
    return ClientService.deleteClient(ctx.state.clients[index]).then(() => {
      ctx.commit('DELETE_CLIENT', index);
    });
  },
  fetchClients(ctx, {
    page,
    limit,
    sortModel
  }) {
    ctx.commit("TOGGLE_LOADING", true);
    const pageClients = ctx.state.clients[page];
    if (pageClients && pageClients.length === limit) {
      return Promise.resolve(pageClients)
    }
    return ClientService.getClients(page, limit, sortModel)
      .then(response => {
        ctx.commit("SET_CLIENTS", {
          page,
          clients: response.data
        });
        ctx.commit("SET_PAGES", Math.ceil(response.headers["x-total-count"] / ctx.state.limit));
      })
      .catch(error => {
        console.log('Произошла ошибка:', error.response)
      })
      .finally(function () {
        ctx.commit("TOGGLE_LOADING", false);
      })
    },
  switchLimit: ({
    commit
  }, payload) => {
    commit('CHANGE_LIMIT', payload);
  },
  searchClient: (ctx, {
    searchModel,
    searchValue,
    currentPage
  }) => {
    ctx.commit("TOGGLE_LOADING", true);
    return ClientService.searchClient(searchModel, searchValue).then((response) => {
      ctx.commit("SET_CLIENTS", {
        currentPage,
        clients: response.data
      });
    }).finally(function () {
      ctx.commit("TOGGLE_LOADING", false);
    });
  },
  /* sortClients:(ctx, { sortModel, currentPage, limit }) => {
    return ClientService.sortClients(sortModel, limit).then((response) => {
      ctx.commit("SET_CLIENTS", {
        currentPage,
        clients: response.data
      });
    }); 
  } */
}