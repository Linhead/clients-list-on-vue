import axios from 'axios';
import { baseFetchURL } from "../consts.js";

const apiClient = axios.create({
  baseURL: baseFetchURL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  timeout: 10000
})

export default {
  getClients(page, limit, sortModel) {
    return apiClient.get(`/clients?_limit=${limit}&_page=${page}&_sort=${sortModel.key}&_order=${sortModel.desc}`)
  },
  postClient(client) {
    return apiClient.post('/clients/', client)
  },
  updateClient(client) {
    return apiClient.patch(`/clients/${client.id}`, client)
  },
  deleteClient(client) {
    return apiClient.delete(`/clients/${client.id}`)
  },
  searchClient(searchModel, searchValue) {
    return apiClient.get(`/clients?${searchModel}_like=^${searchValue}`)
  },
  /* sortClients(sortModel, limit) {
    return apiClient.get(`/clients?_limit=${limit}&_sort=${sortModel.value}&_order=${sortModel.desc}`)
  } */
}
