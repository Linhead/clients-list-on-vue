import Vue from 'vue'
import VueRouter from 'vue-router'
import Clients from '../views/Clients.vue'
import ClientsEdit from '../views/ClientsEdit.vue'
import ClientAdd from '../views/ClientAdd.vue'
import Login from "../views/Login.vue";

Vue.use(VueRouter)

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: '/',
    name: 'Clients',
    component: Clients,
    meta: { requiresAuth: true }
  },
  {
    path: '/edit/:id',
    name: 'ClientsEdit',
    component: ClientsEdit,
    meta: { requiresAuth: true }
  },
  {
    path: '/add',
    ame: 'ClientsAdd',
    component: ClientAdd,
    meta: { requiresAuth: true }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    const authUser = JSON.parse(
      window.localStorage.getItem("currentUser") || "{}"
    );
    if (authUser && authUser.accessToken) {
      next();
    } else {
      next({ name: "Login" });
    }
  } else {
    next(); // make sure to always call next()!
  }
});

export default router